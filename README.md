# Sistema Biblioteca Seminário Temático IV

Projeto criado para apresentação do seminário temático envolvendo as disciplinas Engenharia de Software II, Banco de Dados e Orientação a Objetos do curso de Sistemas de Informação da Faculdade Cest São Luís - MA.

## 🚀 Começando

Faça o clone do nosso projeto na pagina inicial.

### 📋 Pré-requisitos

Para rodar você precisa ter o Java JDK instalado em seu sistema e o Git para realizar o clone do projeto no Windows ou Linux.

## 🛠️ Construído com

* [Maven](https://maven.apache.org/) - Gerente de Dependência
* [Java](https://www.java.com/pt-BR/) - Linguagem de Programação

## 🖇️ Colaborando

Por favor, leia o [COLABORACAO.md](https://gitlab.com/marcioaquilles/sistema-biblioteca-seminario-tematico-iv/-/new/main?commit_message=Add+CONTRIBUTING&file_name=CONTRIBUTING.md) para obter detalhes sobre o nosso código de conduta e o processo para nos enviar pedidos de solicitação.

## 📌 Versão

Nós usamos [GitLab](https://gitlab.com/) para controle de versão. Para as versões disponíveis, observe as [tags neste repositório](https://github.com/suas/tags/do/projeto). 

## ✒️ Autores

* **Márcio Aquilles** - *Trabalho Inicial* - [desenvolvedor](https://gitlab.com/marcioaquilles)
* **Thiago Arouche** - *Telas e Integraçao* - [desenvolvedor](https://gitlab.com/Thiagopr)
* **Yhugo Gabriel Pereira Santos** - *Testes* - [desenvolvedor](https://gitlab.com/yhugo.santos)
* **Cley Gabriel** - *Testes* - [desenvolvedor](https://gitlab.com/cley-gabriel.santos)
* **Lucas Silva Piedade** - *Testes* - [desenvolvedor](https://gitlab.com/lucaspiedade)
* **Jordanilson** - *Testes* - [desenvolvedor](https://github.com/search?q=Jordanilson-github)


## 📄 Licença

Este projeto está sob a licença (MIT License) - veja o arquivo [LICENSE.md](https://gitlab.com/marcioaquilles/sistema-biblioteca-seminario-tematico-iv/-/blob/main/LICENSE) para detalhes.

## 🎁 Expressões de gratidão

* Esse projeto foi gratificante para obter skills e aprimorar nossas habilidades em desenvolvimento Java POO, Criação do Banco de Dados e PLanejamento e  Modelagem do Sistema. 📢
* Obrigado a todos os professores envolvidos:
[Professor](https://gitlab.com/prof_bemanuel_pe) - **Bruno Emanuel** 👨‍💻.
[Professora]() - **Aline Lopes** 😉.
[Professora]() - **Marta Barreiros** 🐱‍💻.

---
